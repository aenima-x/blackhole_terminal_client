import logging
import requests
from .config import ConfigManager
from .exceptions import BlackholeServerError

logger = logging.getLogger(__name__)

class ApiConsumer(object):
    blackhole_server_config = ConfigManager.get_config("blackhole_server")


    @classmethod
    def get_api_url(cls, endpoint):
        return f"{cls.blackhole_server_config.blackhole_server}{endpoint}"

    @classmethod
    def perform_get(cls, url):
        try:
            response = requests.get(url, headers={'Authorization': f"Token {cls.blackhole_server_config.blackhole_token}"})
            return response
        except requests.exceptions.ConnectionError as e:
            raise BlackholeServerError("Connection to Blackhole Server Failed")
        except Exception as e:
            raise BlackholeServerError("Unknown: %s" % e)

    @classmethod
    def perform_post(cls, endpoint, json):
        try:
            response = requests.post(endpoint, json=json, headers={'Authorization': f"Token {cls.blackhole_server_config.blackhole_token}"})
            if response.status_code not in (200, 201):
                raise BlackholeServerError("Error in POST (%s, %s)" % (response.status_code, response.text))
            return response
        except Exception as e:
            raise BlackholeServerError("Post Error: %s" % e)