import logging
import pty
import shlex
import termios
import socket
import signal
import threading
import tty
import sys
import select
import os
import subprocess

import paramiko
import warnings
warnings.filterwarnings(action='ignore',module='.*paramiko.*')
import psutil

from .exceptions import KilledByAdminException


logger = logging.getLogger(__name__)


SSH_TIME_OUT = 3


class Client(object):

    def __init__(self, session):
        self.session = session
        self.killed = False

    def set_killed(self, *args, **kwargs):
        self.killed = True
        self.stop()


class SecureShellClient(Client):

    def __init__(self, session, forward_x=False):
        super().__init__(session)
        self.socket = None
        self.channel = None
        self.transport = None
        self.forward_x = forward_x
        logger.debug(f"Client Created: Forward X: {forward_x}")

    def connect(self, ip, port, size):
        self.terminal_size = size
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(SSH_TIME_OUT)
        self.socket.connect((ip, port))
        logger.debug("Connected to %s:%s", ip, port)

    def get_transport(self):
        logger.debug("Get Transport")
        transport = paramiko.Transport(self.socket)
        transport.set_keepalive(30)
        transport.start_client()
        return transport

    def start(self, user, private_key=None, password=None):
        print(chr(27) + "[2J")
        self.transport = self.get_transport()
        if private_key and  not password:
            logger.debug("Validating with Private Key")
            self.transport.auth_publickey(user, private_key)
        elif password and not private_key:
            logger.debug("Validating with Password")
            self.transport.auth_password(user, password)
        self.start_session(self.transport)

    def start_session(self, transport):
        logger.debug("Start Session")
        self.channel = transport.open_session()
        cols, rows = self.terminal_size
        self.channel.get_pty('xterm', cols, rows)
        self.channel.invoke_shell()
        self.interactive_shell(self.channel)

    def interactive_shell(self, chan):
        sys.stdout.flush()
        try:
            signal.signal(signal.SIGHUP, self.stop)
            signal.signal(signal.SIGQUIT, self.set_killed)
            oldtty = termios.tcgetattr(sys.stdin)
            tty.setraw(sys.stdin.fileno())
            tty.setcbreak(sys.stdin.fileno())
            chan.settimeout(0.0)
            if self.forward_x:
                self.start_forward_x(chan)
            while True:
                r, w, e = select.select([chan, sys.stdin], [], [])
                if chan in r:
                    try:
                        c = chan.recv(1024)
                        if len(c) == 0:
                            break
                        sys.stdout.write(c.decode('utf-8', 'ignore'))
                        sys.stdout.flush()
                    except socket.timeout:
                        break
                if sys.stdin in r:
                    c = os.read(sys.stdin.fileno(), 1)
                    if len(c) == 0:
                        break
                    chan.send(c)
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, oldtty)
        except select.error as e:
            logger.error(str(e))
            pass
        except Exception as e:
            logger.error(str(e))
            pass

    def start_forward_x(self, chan):
        display_data = os.environ.get('DISPLAY', False)
        if display_data:
            display_number, display_visual_number = display_data.strip().split(':')[1].split('.')
            keys = subprocess.check_output(['xauth', 'list']).decode('utf-8')
            for key_entry in keys.splitlines():
                host_display, key_type, key = key_entry.split()
                if host_display.endswith(f"{display_number}"):
                    chan.send("export DISPLAY=%s\n" % display_data)
                    key_command = "xauth add :%s %s %s\n" % (display_number, key_type, key)
                    chan.send(key_command)
                    local_X_port = 6000 + int(display_number)
                    def start_X_forward(channel, origin, destination):
                        thr = threading.Thread(target=self.handle_X_connection, args=(channel, 'localhost', local_X_port))
                        thr.setDaemon(True)
                        thr.start()
                    self.transport = chan.get_transport()
                    self.transport.request_port_forward('', local_X_port, start_X_forward)
                    break

    def handle_X_connection(self, chan, host, port):
        logger.debug("Staring X Forward")
        sock = socket.socket()
        try:
            sock.connect((host, port))
        except Exception as e:
            logger.error(u'Forwarding request to %s:%d failed: %r' % (host, port, e))
            return
        logger.debug(u'Connected!  Tunnel open %r -> %r -> %r' % (chan.origin_addr,
                                                                chan.getpeername(), (host, port)))
        while True:
            r, w, x = select.select([sock, chan], [], [])
            if sock in r:
                data = sock.recv(1024)
                if len(data) == 0:
                    break
                chan.send(data)
            if chan in r:
                data = chan.recv(1024)
                if len(data) == 0:
                    break
                sock.send(data)
        chan.close()
        sock.close()
        logger.debug(u'Tunnel closed from %r' % (chan.origin_addr,))

    def stop(self, *args, **kwargs):
        print(chr(27) + "[2J") # Clear the screen
        if not args:
            if self.channel:
                self.channel.close()
            if self.transport:
                self.transport.close()
            if self.socket:
                self.socket.close()
            if self.killed:
                logger.debug("Killed session")
                raise KilledByAdminException()


class DatabaseClient(Client):

    def __init__(self, session):
        super().__init__(session)
        self.pid = os.getpid()
        logger.debug("Client Created")

    def connect(self, ip, port):
        # Just check if network is available before execute command
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((ip, port))
        if result != 0:
            logger.error("Database Timeout: %s:%s")
            raise socket.timeout("Time out")
        else:
            logger.debug("Connected to %s:%s" % (ip, port))

    def start(self):
        print(chr(27) + "[2J")
        try:
            self.start_session()
        except Exception as e:
            logger.error(e)
            self.stop()
            raise e

    def stop(self, *args, **kwargs):
        print(chr(27) + "[2J")
        if not args:
            if self.killed:
                for p in psutil.process_iter():
                    if p.ppid() == self.pid:
                        logger.debug("Kill process %s" % p.pid)
                        os.kill(p.pid, 9)
                raise KilledByAdminException()

    def start_session(self):
        signal.signal(signal.SIGHUP, self.stop)
        signal.signal(signal.SIGQUIT, self.set_killed)
        sys.stdout.flush()
        database_command = self.session.get_connection_command()
        logger.debug("Execute Databse Command: %s", database_command)
        pty.spawn(shlex.split(database_command), sys.stdout.read, sys.stdin.read)
        self.stop()
