import getpass
import logging
import os
import configparser
from .exceptions import ConfigError


DEFAULT_CONFIG_FILE = "/etc/blackhole/blackhole_terminal_client.ini"
DEFAULT_LOGGER = "blackhole_terminal"


class ConfigManager(object):
    config_modules = {}

    @classmethod
    def get_config(cls, name):
        return cls.config_modules.get(name, None)

    @classmethod
    def add_config(cls, section_name, config):
        if section_name not in cls.config_modules:
            cls.config_modules[section_name] = config
        else:
            raise ConfigError(f"Config section {section_name} already exists")


class Config(object):
    section_name = ""

    def __init__(self):
        ConfigManager.add_config(self.section_name, self)

    def __str__(self):
        return f"[{self.__class__.__name__} - {self.section_name}]"

    def parse(self, section):
        try:
            for attribute in self.__dict__:
                if attribute in section:
                    value = section.get(attribute)
                    if isinstance(self.__dict__[attribute], bool):
                        string_bool_value = value.lower()
                        if string_bool_value in ("true", "yes"):
                            self.__setattr__(attribute, True)
                        elif string_bool_value in ("false", "no"):
                            self.__setattr__(attribute, False)
                        else:
                            raise Exception(f"{self.__class__.__name__}: {value} is not a Boolean")
                    elif isinstance(self.__dict__[attribute], int):
                        if value.isdigit():
                            self.__setattr__(attribute, int(value))
                        else:
                            raise Exception(f"{self.__class__.__name__}: {value} is not an Integer")
                    else:
                        self.__setattr__(attribute, value)
        except Exception as e:
            raise ConfigError(f"{self.__class__.__name__}: ERROR parsing config - {e}")
        else:
            try:
                self.action_after_parse()
            except Exception as e:
                raise ConfigError(f"{self.__class__.__name__}: ERROR after parse config - {e}")

    def print_config(self):
        print(self)
        for attribute in self.__dict__:
            print(f"-> {attribute}: {self.__getattribute__(attribute)}")
        print("")

    def action_after_parse(self):
        pass

    @staticmethod
    def read_config(config):
        for section_name in config:
            if section_name == "DEFAULT":
                continue
            configuration = ConfigManager.get_config(section_name)
            if configuration:
                configuration.parse(config[section_name])
            else:
                print(f"Unknown configuration ({section_name})")


class ClientConfig(Config):
    section_name = "client"

    def __init__(self):
        super().__init__()
        self.timezone = "UTC"
        self.session_log_path = "/tmp/"

    def action_after_parse(self):
        if not os.path.isdir(self.session_log_path):
            raise ConfigError(f"Session Log Path ({self.session_log_path}) invalid")


class LoggerConfig(Config):
    section_name = "logger"

    def __init__(self):
        super().__init__()
        self.log_level = "INFO"
        self.log_handler = "file"
        self.log_path = "/tmp"
        self.log_format = '%(asctime)-15s: - Blackhole Client: [%(levelname)s] - %(name)s: %(message)s'

    def action_after_parse(self):
        from logging.handlers import SysLogHandler
        logger = logging.getLogger(DEFAULT_LOGGER)
        int_log_level = getattr(logging, self.log_level, logging.INFO)
        logger.setLevel(int_log_level)
        if self.log_handler.lower() == "syslog":
            log_handler = SysLogHandler(address='/dev/log', facility=22)
        else:
            if not os.path.isdir(self.log_path):
                raise ConfigError(f"Log Path ({self.log_path}) invalid")
            else:
                log_file = os.path.join(self.log_path, f"blackhole_client_{getpass.getuser()}.log")
                log_handler = logging.FileHandler(log_file)
        log_handler.setFormatter(logging.Formatter(self.log_format))
        logger.addHandler(log_handler)


class BlackholeServerConfig(Config):
    section_name = "blackhole_server"

    def __init__(self):
        super().__init__()
        self.blackhole_server = "http://localhost:8000"
        self.blackhole_token = "token"

    def action_after_parse(self):
        if self.blackhole_server.endswith("/"):
            self.blackhole_server = self.blackhole_server[:-1]


class RedisConfig(Config):
    section_name = "redis"

    def __init__(self):
        super().__init__()
        self.server = "localhost"
        self.port = 6379
        self.streaming_session_name = "sessions.%s"


def load_config():
    if os.path.isfile(DEFAULT_CONFIG_FILE):
        file_config = configparser.ConfigParser(interpolation=None)
        file_config.read(DEFAULT_CONFIG_FILE)
        try:
            Config.read_config(file_config)
        except ConfigError:
            raise
        except Exception as e:
            raise ConfigError(str(e))
    else:
        raise ConfigError(f"Config File ({DEFAULT_CONFIG_FILE}) not exists")


def dump_config():
    print("Blackhole Client [CONFIG]")
    for config in ConfigManager.config_modules.values():
        config.print_config()

# Generate configurations
ClientConfig()
LoggerConfig()
BlackholeServerConfig()
RedisConfig()


load_config()
