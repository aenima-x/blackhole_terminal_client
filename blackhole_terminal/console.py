import sys
import argparse
import logging

from . import VERSION
from .config import dump_config
from .engine import Engine


logger = logging.getLogger(__name__)


class Console(object):
    def __init__(self):
        self.engine = Engine()

    def run(self):
        self.engine.run()


def launch():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--version", help="Get version", action="store_true", default=False)
    parser.add_argument("--dump-config", help="Dump Configuration", action="store_true", default=False)
    args = parser.parse_args()
    if args.version:
        print(f"Blackhole Client: Version {VERSION}")
        sys.exit(0)
    if args.dump_config:
        print(f"Blackhole Client: Version {VERSION}")
        dump_config()
        sys.exit(0)
    console = Console()
    console.run()
