import getpass
import json
import logging
import uuid
from datetime import datetime
import os
import stat

from .config import ConfigManager
from .watcher import StdOutHandler, StdInHandler
from .idm_server import UserProxy
from .exceptions import UnknownUser, BlackholeServerError
from .session import Session


logger = logging.getLogger(__name__)


class Engine(object):
    CONNECTION_TYPE_SSH = "SSH"
    CONNECTION_TYPE_DB = "DB"

    def __init__(self):
        self.stdout_handler = StdOutHandler()
        self.session_uuid = str(uuid.uuid4())
        self.user = None
        self.pid = os.getpid()
        self.ppid = os.getppid()
        self.os_username = getpass.getuser()
        self.session_record_id = None
        try:
            self.source_ip, _, self.source_port = os.environ.get('SSH_CLIENT').split()
        except:
            self.source_ip, self.source_port = '0.0.0.0 0'.split()


    def __str__(self):
        return f"[auth] user={self.user} from={self.source_ip}:{self.source_port} pid={self.pid} session_uuid={self.session_uuid}"

    def run(self):
        initial_error_message = None
        try:
            self.user = UserProxy(self.os_username)
            self.user.search_user()
        except UnknownUser:
            logger.error("Unknown User: %s", self.os_username )
            initial_error_message = {"title": "Unknown User", "message": f"[{self.os_username}] is not a valid Blackhole User"}
        except BlackholeServerError as e:
            logger.error("Blackhole Server Error: %s", e)
            initial_error_message = {"title": "Error", "message": f"Blackhole Server Error: {e}"}
        except ConnectionError as e:
            logger.error("Connection Error with Blackhole Server (%s)", e)
            initial_error_message = {"title": "Error", "message": f"Blackhole Server Error: {e}"}
        except Exception as e:
            logger.error("Unknown Error in search_user (%s)", e)
            initial_error_message = {"title": "Error", "message": f"Unknown Error: {e}"}
        else:
            logger.info(self)
            if not self.user.enabled:
                logger.warning("The user %s is disabled", self.os_username)
                initial_error_message = {"title": "Disabled User", "message": f"[{self.os_username}] user is Disabled"}
        from .gui import Gui
        self.gui = Gui(self)
        self.gui.start(initial_error_message)

    def start_connection(self, target_connection):
        logger.info("Start connection: %s", target_connection)
        if self.validate_connection(target_connection):
            logger.debug("Connection Allowed")
            try:
                session = Session.get_session(self, target_connection)
            except Exception as e:
                raise BlackholeServerError(str(e))
            try:
                session.start()
            except Exception as e:
                logger.error("Session Error: %s", e)
                raise
            finally:
                self.stdout_handler.restore()
                self.gui.restore()
        else:
            logger.debug("Connection Not Allowed")
            self.gui.show_notification("Connection not Allowed")

    def validate_connection(self, target_connection):
        return self.user.is_allowed_to_connect(target_connection.id)

    def session_started_handler(self, session):
        log_file = self.get_log_file(session.target_connection)
        self.stdout_handler.capture(self.session_uuid, log_file)
        logger.debug("Start session handler")
        self.session_record_id = self.user.generate_session_record(self.session_uuid, self.pid, self.source_ip, log_file,
                                                                   session.target_connection)
        logger.debug("Session Record generated")
        self.gui.pause()

    def session_ended_handler(self, session):
        try:
            logger.debug("Stop session handler")
            self.user.finalize_session_record(self.session_record_id)
            self.session_record_id = None
        except Exception as e:
            logger.error(e)

    def get_terminal_size(self):
        return self.gui.loop.screen.get_cols_rows()

    def get_log_file(self, target_connection):
        client_config = ConfigManager.get_config("client")
        session_log_path = client_config.session_log_path
        user_logs_path = os.path.join(session_log_path, self.user.username)
        if not os.path.isdir(user_logs_path):
            os.mkdir(user_logs_path)
            os.chmod(user_logs_path, stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH)
        real_user = self.user.username
        target_name = target_connection.name
        actual_user = target_connection.target_user
        connection_type = target_connection.target.connection_type
        session_uuid = self.session_uuid
        log_date = datetime.now().strftime("%d%m%Y.%H%M%S")
        file_name = f"{real_user}_{connection_type}_{target_name}_{actual_user}_{log_date}_{self.pid}_{session_uuid}.log"
        log_file_name = os.path.join(user_logs_path, file_name)
        return log_file_name
