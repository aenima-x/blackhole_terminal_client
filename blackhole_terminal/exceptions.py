
class ConnectionError(Exception):
    pass

class ConfigError(Exception):
    pass


class BlackholeServerError(Exception):
    pass


class UnknownUser(Exception):
    pass


class KilledByAdminException(Exception):

    def __init__(self, *args, **kwargs):
        super().__init__(self, "Session Killed by an Administrator")