import logging
from collections import namedtuple

import urwid
import urwid.curses_display
import time
from . import VENDOR, VERSION
from .exceptions import KilledByAdminException

logger = logging.getLogger(__name__)


_environments_cache = {}
_environments_cache_index = []

X_FORWARD_KEYS = ("X",)
MANUAL_SSH_KEYS = ("S",)
HELP_KEYS = ("H",)
SEARCH_KEYS = ("F",)
QUIT_KEYS = ("Q", "q")
EXPAND_KEYS = (" ", "enter")
ESC_KEYS = ("esc",)

SPLASH_SCREEN_TIME = 2
### environments cache functions


def store_initial_environment(initial, end):
    """Store the initial current working directory path components."""
    global _initial_environment, _end_environment
    _initial_environment = initial # Nombre del primer environment
    _end_environment = end # nombre del segundo environment


def get_environment_from_cache(name):
    """Return the Directory object for a given path.  Create if necessary."""
    return _environments_cache[name]


def get_next_environment_from_cache(name):
    index = _environments_cache_index.index(name)
    if index < len(_environments_cache_index) - 1:
        return _environments_cache[_environments_cache_index[index + 1]]
    return False


def get_prev_environment_from_cache(name):
    index = _environments_cache_index.index(name)
    if index > 0:
        return _environments_cache[_environments_cache_index[index - 1]]
    return False


def set_environments_cache(environment, target_connections, gui):
    """
    en _environments_cache guarda los evironmentStore y usa _environments_cache_index como indice para acceder en forma directa
    """
    _environments_cache[environment.name] = EnvironmentStore(environment, target_connections, gui)
    _environments_cache_index.append(environment.name)


### PopUps
class NotificationPopUp(urwid.WidgetWrap):
    """A dialog that appears with nothing but a close button """
    signals = ["close"]

    def __init__(self, message):
        close_button = urwid.Button("OK")
        urwid.connect_signal(close_button, "click", lambda button: self._emit("close"))
        pile = urwid.Pile([urwid.Text(message, align="center"),
                           urwid.Padding(close_button, align="center", left=13, right=13)])
        fill = urwid.Filler(pile)
        super().__init__(urwid.AttrMap(urwid.LineBox(fill, title="Notification"), "notification"))


class NotificationPopUpLauncher(urwid.PopUpLauncher):
    def __init__(self, gui):
        self.gui = gui
        self.overlay_width = 35
        self.overlay_height = 7
        super().__init__(urwid.Text("", align="right"))
        self._message = None

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value):
        self._message = value

    def create_pop_up(self):
        pop_up = NotificationPopUp(self._message)
        urwid.connect_signal(pop_up, "close",
                             lambda button: self.close_pop_up())
        return pop_up

    def get_pop_up_parameters(self):
        w, h = self.gui.screen.get_cols_rows()
        left = -1 * (w - ((w - self.overlay_width)/2) - 1)
        top = (h - self.overlay_height)/2
        return {"left": left, "top": top, "overlay_width": 35, "overlay_height": 7}

    def show_indicator(self, message):
        self.original_widget.set_text(message)


class HelpPopUp(urwid.WidgetWrap):
    """Help Pop Up"""
    signals = ["close"]

    def __init__(self):
        close_button = urwid.Button("OK")
        urwid.connect_signal(close_button, "click", lambda button: self._emit("close"))
        help_text = [("footer_msg", " - Move: "), ("key", "up, down, home, end, left, w/mouse\n"),
                     ("footer_msg", " - Expand: "), ("key", "space, click\n"),
                     ("footer_msg", " - Select: "), ("key", "enter\n"),
                     ("footer_msg", " - Export X: "), ("key", "X (shift + x)\n"),
                     ("footer_msg", " - Search: "), ("key", "F (shift + f)\n"),
                     ("footer_msg", " - Quit: "), ("key", "Q\n\n"),
                     ("footer_msg", " - By: "), ("key", f"{VENDOR}\n"),
                     ("footer_msg", " - Version: "), ("key", VERSION)]
        pile = urwid.Pile([urwid.Text(help_text, align="left"),
                           urwid.Padding(close_button, align="center", left=20, right=20)])
        fill = urwid.Filler(pile)
        super().__init__(urwid.AttrMap(urwid.LineBox(fill, title="Help"), "help"))

    def keypress(self, size, key):
        if key in ESC_KEYS:
            self._emit("close")


class HelpPopUpLauncher(urwid.PopUpLauncher):
    """Button to launch the help pop up """
    def __init__(self, gui):
        self.gui = gui
        self.overlay_width = 50
        self.overlay_height = 12
        super().__init__(urwid.Text("help(H)", align="right"))

    def create_pop_up(self):
        pop_up = HelpPopUp()
        urwid.connect_signal(pop_up, "close", lambda button: self.close_pop_up())
        return pop_up

    def get_pop_up_parameters(self):
        w, h = self.gui.screen.get_cols_rows()
        left = -1 * (w - ((w - self.overlay_width)/2) - 8)
        top = -1 * (h - (h - self.overlay_height)/2)
        return {"left": left, "top": top, "overlay_width": self.overlay_width, "overlay_height": self.overlay_height}


class TargetSearchPopUpLauncher(urwid.PopUpLauncher):
    """Button to launch the help pop up """
    def __init__(self, gui):
        self.gui = gui
        w, h = self.gui.engine.get_terminal_size()
        # TODO calcularlo
        self.overlay_width = int(w * 0.5)
        self.overlay_height = int(h * 0.5)
        super().__init__(urwid.Text("search(F)", align="right"))

    def create_pop_up(self):
        pop_up = TargetSearchPopUp(self.gui, self.overlay_width, self.overlay_height)
        urwid.connect_signal(pop_up, "close", lambda button: self.close_pop_up())
        return pop_up

    def get_pop_up_parameters(self):
        w, h = self.gui.screen.get_cols_rows()
        self.overlay_width = int(w * 0.5)
        self.overlay_height = int(h * 0.5)
        left = -1 * ((w - self.overlay_width)/4 + self.overlay_width)
        top = -1 * (h - (h - self.overlay_height)/2)
        return {"left": left, "top": top, "overlay_width": self.overlay_width, "overlay_height": self.overlay_height}


class SearchResultWidget(urwid.Text):
    def __init__(self, result, *args, **kwargs):
        self.target_connection = result
        super().__init__(f"{result.name} ({result.ip}) [{result.target.connection_type}]", *args, **kwargs)
        self.state = False

    def selectable(self):
        return True

    def keypress(self,size, key):
        if key == "enter":
            self.state = True
        return key

    def mouse_event(self, size, event, button, col, row, focus):
        if event == 'mouse release':
            self.state = True
        return False

    def get_state(self):
        return self.state

    def get_label(self):
        text, attr = self.get_text()
        return text


class TargetSearchPopUp(urwid.WidgetWrap):
    """Target Search Pop Up"""
    signals = ["close"]

    def __init__(self, gui, w, h):
        self.gui = gui
        self.results_list_walker = urwid.SimpleFocusListWalker([])
        self.results_listbox = urwid.BoxAdapter(urwid.AttrWrap(urwid.ListBox(self.results_list_walker), "result_selectable"), h - 10)
        self.search_edit = urwid.Edit("Name: ")
        self.error_message = urwid.Text(("notification", ""), align="center")
        self.pile = urwid.Pile([self.search_edit,
                           urwid.Padding(self.results_listbox, align="center", width=("relative", 98)),
                           self.error_message,
                ], focus_item=0
                               )
        search_window = urwid.AttrMap(urwid.LineBox(urwid.Filler(self.pile), title="Search"), 'help')
        super().__init__(search_window)

    def start_connection(self, target):
        try:
            self.gui.start_connection(target)
        except Exception as e:
            self.error_message.set_text(str(e))
        else:
            self._emit("close")

    def search_target(self, search_string):
        MIN_LETTERS = 3
        if len(search_string) >= MIN_LETTERS:
            self.results_list_walker.clear()
            results = list(map(lambda x: urwid.AttrWrap(SearchResultWidget(x), "result_selectable", f"{x.target.connection_type}_focus"), filter(lambda x: search_string in x.name,
                             [tc for e in self.gui.engine.user.get_environments() for tc in e.get_target_connections()])))
            if results:
                self.results_list_walker.extend(results)
            else:
                self.error_message.set_text(("notification", "No matches!!"))
        else:
            self.error_message.set_text(("notification", "Enter at least %s letters" % MIN_LETTERS))
            self.results_list_walker.clear()

    def keypress(self, size, key):
        if key in ESC_KEYS:
            self._emit("close")
        elif key == "enter":
            self.error_message.set_text(("notification", ""))
            if isinstance(self.pile.get_focus(), urwid.Edit):
                text = self.search_edit.edit_text
                self.search_target(text)
            else:
                focus_widget, index = self.results_list_walker.get_focus()
                if focus_widget:
                    self.start_connection(focus_widget.original_widget)
            return None
        else:
            return super().keypress(size, key)

    def unhandled_key(self, size, k):
        logger.info("Unhanled: %s", k)



class ManulSSHPopUpLauncher(urwid.PopUpLauncher):
    """Button to launch the manual ssh connection pop up """
    def __init__(self, gui):
        self.gui = gui
        self.overlay_width = 50
        self.overlay_height = 12
        super().__init__(urwid.Text("ssh(S)"))

    def create_pop_up(self):
        pop_up = ManualSSHPopUp(gui=self.gui)
        urwid.connect_signal(pop_up, "close", lambda button: self.close_pop_up())
        return pop_up

    def get_pop_up_parameters(self):
        w, h = self.gui.screen.get_cols_rows()
        left = -1 * (w - ((w - self.overlay_width)/2) - 8)
        top = -1 * (h - (h - self.overlay_height)/2)
        return {"left": left, "top": top, "overlay_width": self.overlay_width, "overlay_height": self.overlay_height}


class ManualSSHPopUp(urwid.WidgetWrap):
    """Manual connection Pop Up"""
    signals = ["close"]

    def __init__(self, gui):
        self.gui = gui
        cancel_button = urwid.Button("Cancel")
        login_button = urwid.Button("Login")
        urwid.connect_signal(cancel_button, "click", lambda button: self._emit("close"))
        urwid.connect_signal(login_button, "click", self.start_connection)
        self.host_edit = urwid.Edit("Host:")
        self.port_edit = urwid.IntEdit("Port:", default=22)
        self.user_edit = urwid.Edit("User:")
        self.password_edit = urwid.Edit("Password:", mask="*")
        self.forward_x_checkbox = urwid.CheckBox("Export X")
        self.error_message = urwid.Text(("notification", ""), align="center")
        pile = urwid.Pile([self.host_edit, self.port_edit, self.user_edit, self.password_edit, self.forward_x_checkbox,
                          self.error_message,
                          urwid.Padding(urwid.Columns([(9, login_button), (10, cancel_button)], dividechars=1),
                                        align="center", width=("relative", 50))])
        manual_ssh_window = urwid.AttrMap(urwid.LineBox(urwid.Filler(pile), title="Manual SSH"), 'help')
        super().__init__(manual_ssh_window)

    def start_connection(self, button):
        ip = self.host_edit.get_edit_text()
        port = self.port_edit.value()
        username = self.user_edit.get_edit_text()
        password = self.password_edit.get_edit_text()
        if not ip or not username or not password:
            self.error_message.set_text(("notification", "All fields are required!!"))
        else:
            fake_target_connection = self.gui.engine.user.profile.get_manual_target_connection(ip, ip, port, username, password, "SSH")
            fake_target_connection.forward_x = self.forward_x_checkbox.get_state()
            try:
                self.gui.engine.start_connection(fake_target_connection)
            except Exception as e:
                self.error_message.set_text(str(e))
            else:
                self._emit("close")

    def keypress(self, size, key):
        if key in ESC_KEYS:
            self._emit("close")
        else:
            return super().keypress(size, key)

### Widgets


class EnvironmentStore(object):
    """
    Stores the environment and its hosts as Tree widgets
    * environments: environment object
    """
    def __init__(self, environment, target_connections, gui):
        """
        Constructor.
        Arguments:
        user -- User Object
        environment -- Environment Object
        """
        self.gui = gui
        self.environment = environment
        self.environment_w = EnvironmentTree(self.environment, len(target_connections), gui)
        self.widgets = {}
        self.widgets_index = []
        self.set_targets(target_connections)

    def set_targets(self, target_connections):
        """Crea los objetos equipos.
        Tengo que pasar una lista, que contenga una lista con los equipos de cada ambiente.
        y en el index 0 el ambiente"""
        j = 0
        for target_connection in target_connections:
            key = f"{target_connection.target.name}_{target_connection.id}"
            if key not in self.widgets:
                target_tree = TargetTree(self.environment, target_connection, j, self.gui)
                urwid.connect_signal(target_tree, "connect", self.gui.start_connection)
                self.widgets[key] = target_tree
                self.widgets_index.append(key)
                j += 1

    def get_widget(self, name):
        """Return the widget for a given host."""
        if name == self.environment.name:
            return self.environment_w
        else:
            return self.widgets[name]

    def next_inorder_from(self, index):
        """Return the Widget following index depth first."""
        if index < len(self.widgets_index) - 1:
                index += 1
                return self.get_widget(self.widgets_index[index])
        else:
            nextEnvironment = get_next_environment_from_cache(self.environment.name)
            if nextEnvironment:
                return nextEnvironment.environment_w
            else:
                return None

    def prev_inorder_from(self, index):
        """Return the TreeWidget preceeding index depth first."""
        if index > 0:
            index -= 1
            return self.get_widget(self.widgets_index[index])
        else:
            return self.environment_w

    def get_first(self):
        """Return the first TreeWidget in the directory."""
        if self.environment_w.expanded:
            return self.get_widget(self.widgets_index[0])
        else:
            return self.environment_w

    def get_last(self):
        """Return the last TreeWIdget in the directory."""
        if self.environment_w.expanded:
            return self.get_widget(self.widgets_index[-1])
        else:
            return self.environment_w


class EnvironmentWalker(urwid.ListWalker):
    def __init__(self, gui):

        self.gui = gui
        environments = self.gui.engine.user.get_environments()
        if len(environments) > 0:
            first_environment_name = environments[0].name
            last_environment_name = environments[-1].name
            store_initial_environment(first_environment_name, last_environment_name)
            for environment in environments:
                set_environments_cache(environment, environment.get_target_connections(), gui)
            widget = get_environment_from_cache(first_environment_name).environment_w
            self.focus = first_environment_name, widget.target_id
        else:
            pass

    def get_focus(self):
        """
        get the widget in focus
        """
        if not len(_environments_cache):
            return None, None
        else:
            parent, target_id = self.focus
            widget = get_environment_from_cache(parent).get_widget(target_id)
            return widget, self.focus

    def set_focus(self, focus):
        """
        set focus in especific widget
        """
        parent, target_id = focus
        self.focus = parent, target_id
        self._modified()
        self.gui.update_header_text()
        self.gui.update_status_text()

    def get_next(self, start_from):
        """
        get next widget
        """
        parent, target_id = start_from
        widget = get_environment_from_cache(parent).get_widget(target_id)
        target = widget.next_inorder()
        if target is None:
            return None, None
        return target, (target.environment, target.target_id)

    def get_prev(self, start_from):
        """
        get previous widget
        """
        parent, target_id = start_from
        widget = get_environment_from_cache(parent).get_widget(target_id)
        target = widget.prev_inorder()
        if target is None:
            return None, None
        return target, (target.environment, target.target_id)


class TreeWidget(urwid.WidgetWrap):
    def __init__(self, environment, name, target_id, index, description, gui):
        self.last_click = None
        self.gui = gui
        self._selectable = True
        self.environment = environment
        self.name = name
        self.target_id = target_id
        self.index = index
        self.description = description
        w = urwid.AttrWrap(self.widget, None)
        self.__super.__init__(w)
        self.selected = False
        self.update_w()

    @property
    def focus_style(self):
        return self._focus_style

    def selectable(self):
        return self._selectable

    def keypress(self, size, key):
        """Toggle selected on space, ignore other keys."""
        return key

    def update_w(self):
        """
        Update the attributes of wrapped widget based on self.selected.
        """
        self._w.attr = "body"
        self._w.focus_attr = self.focus_style

    def first_child(self):
        """Default to have no children."""
        return None

    def last_child(self):
        """Default to have no children."""
        return None

    def next_inorder(self):
        """Return the next TreeWidget depth first from this one."""
        return get_environment_from_cache(self.environment).next_inorder_from(self.index)

    def prev_inorder(self):
        """Return the previous TreeWidget depth first from this one."""
        return get_environment_from_cache(self.environment).prev_inorder_from(self.index)


class EnvironmentTree(TreeWidget):

    def __init__(self, environment, targets_count, gui):
        self._focus_style = "focus"
        self.widget = urwid.Text([""])
        self.__super.__init__(environment.name, environment.name, environment.name, 0, environment.name, gui)
        self.expanded = False
        self.number_of_targets = targets_count
        self.update_widget()

    def next_inorder(self):
        if not self.expanded:
            next_environment = get_next_environment_from_cache(self.target_id)
            if next_environment:
                return next_environment.environment_w
            else:
                return None
        else:
            return self.first_child()

    def prev_inorder(self):
        prev_environment = get_prev_environment_from_cache(self.target_id)
        if prev_environment:
            if not prev_environment.environment_w.expanded:
                return prev_environment.environment_w
            else:
                return prev_environment.environment_w.last_child()

    def update_widget(self):
        """Update display widget text."""
        if self.expanded:
            mark = "[-]"
        else:
            mark = "[+]"
        self.widget.set_text(["", ("environmentMark", mark), " ",
                              f"{self.description} - Targets: {self.number_of_targets}"])

    def keypress(self, size, key):
        """Handle expand & collapse requests."""
        if key in EXPAND_KEYS:
            self.expanded = not self.expanded
            self.update_widget()
            return None
        else:
            return key

    def mouse_event(self, size, event, button, col, row, focus):
        if event != "mouse press" or button != 1:
            return False
        if row == 0 and col < 3:
            self.expanded = not self.expanded
            self.update_widget()
            return True
        else:
            if self.last_click:
                now = time.time()
                i = now - self.last_click
                if i < 0.6:
                    self.expanded = not self.expanded
                    self.update_widget()
                    self.last_click = None
                else:
                    self.last_click = now
            else:
                self.last_click = time.time()
        return False

    def first_child(self):
        """Return first child if expanded."""
        if not self.expanded:
            return get_environment_from_cache(self.environment).environment_w
        return get_environment_from_cache(self.environment).get_first()

    def last_child(self):
        """Return last child if expanded."""
        if not self.expanded:
            return get_environment_from_cache(self.environment).environment_w
        else:
            return get_environment_from_cache(self.environment).get_last()

    @property
    def selected_text(self):
        return self.description


class TargetTree(TreeWidget):
    signals = ["connect"]

    def __init__(self, environment, target_connection, index, gui):
        self.target_connection = target_connection
        self._focus_style = f"{self.target_connection.target.connection_type}_focus"
        self._widget_text = [f"  {self.description_text}"]
        self.widget = urwid.Text(self._widget_text)
        target_id = f"{self.target_connection.target.name}_{self.target_connection.id}"
        super().__init__(environment.name, self.target_connection.target.name, target_id, index, self.description_text, gui)

    @property
    def description_text(self):
        if self.target_connection.target.description:
            target_text = f"{self.target_connection.target.name} - {self.target_connection.target.description}"
        else:
            target_text = f"{self.target_connection.target.name}"
        return target_text

    def keypress(self, size, key):
        """Handle expand & collapse requests."""
        if key == EXPAND_KEYS[1] or key in X_FORWARD_KEYS:
            if self.target_connection.target.connection_type == self.gui.engine.CONNECTION_TYPE_SSH:
                self.target_connection.forward_x = key in X_FORWARD_KEYS
            self._emit("connect")
            self.widget.set_text(self._widget_text)
            return None
        elif key == "left":
            self.gui.listbox.body.set_focus((self.environment, self.environment))
        else:
            return key

    def mouse_event(self, size, event, button, col, row, focus):
        if event != "mouse press" or button != 1:
            return False
        if self.last_click:
            now = time.time()
            i = now - self.last_click
            if i < 0.6:
                self.last_click = None
                self.target_connection.forward_x = False
                self._emit("connect")
                self.widget.set_text(self._widget_text)
                return False
            else:
                self.last_click = now
        else:
            self.last_click = time.time()
        return False

    @property
    def selected_text(self):
        return self.name


# Main UI

class Gui(object):
    def __init__(self, engine):
        logger.debug("Create Window")
        self.engine = engine
        self.header_text = ""
        self.statusbar_text = ""
        self.ui_is_paused = False
        self.screen = urwid.raw_display.Screen()
        self.loop = None
        self.set_main_palette()

    def set_main_palette(self):
        self.main_palette = [
            ("body", "black", "light gray"),  # Normal Text
            ("focus", "light green", "black", "standout"),  # Focus
            ("head", "white", "dark gray", "standout"),  # Header
            ("foot", "light gray", "dark gray"),  # Footer Separator
            ("key", "light green", "dark gray", "bold"),
            ("environmentMark", "dark blue", "light gray", "bold"),
            ("footer_msg", "yellow", "dark gray"),
            ("notification", "white", "dark red"),
            ("help", "light green", "dark gray"),
            ("DB", "dark magenta", "light gray"),
            ("DB_focus", "white", "dark magenta", "standout"),  # Focus
            ("SSH", "dark blue", "light gray", "underline"),
            ("SSH_focus", "white", "dark blue", "standout"),  # Focus
            ("buttons_pane", "light green", "dark gray",),
            ("status_message", "yellow", "dark gray",),
            ("splash_logo", "black", "light gray", ),
            ("splash_company", "dark red", "light gray", "bold"),
            ('result_selectable', 'black', 'white'),
            ('result_selected', 'white', 'dark blue')
        ]

    def update_header_text(self):
        if hasattr(self, "listbox"):
            focus, _ign = self.listbox.body.get_focus()
            if focus:
                selected_text = focus.selected_text
            else:
                selected_text = "No targets available"
            self.header_text = [("key", "BlackHole"), " ",
                                ("footer_msg", "User: "),
                                ("key", f"{self.engine.user.get_full_name()}"), " ",
                                ("footer_msg", "- Selected: "),
                                ("key", selected_text)]
            self.header_widget.set_text(self.header_text)

    def update_status_text(self):
        if hasattr(self, "listbox"):
            focus, _ign = self.listbox.body.get_focus()
            if type(focus) == TargetTree:
                status_text = [("footer_msg", "Method: "), ("key", focus.target_connection.target.connection_type),
                               ("footer_msg", " - As user: "), ("key", focus.target_connection.target_user)]
                self.statusbar_widget.set_text(status_text)
            else:
                self.statusbar_widget.set_text("")

    def update_header_and_status(self):
        self.update_header_text()
        self.update_status_text()

    def setup(self, loop, initial_error_message):
        logger.debug("Setup %s", initial_error_message)
        if initial_error_message:
            tile = initial_error_message.get("title", "Title")
            message = initial_error_message.get("message", "Message")
            self.draw_error_ui(title=tile, message=message)
        else:
            self.draw_main_ui()

    def draw_splash_screen(self, initial_error_message):
        logo = """
 _     _            _    _           _
| |__ | | __ _  ___| | _| |__   ___ | | ___
| '_ \| |/ _` |/ __| |/ / '_ \ / _ \| |/ _ \\
| |_) | | (_| | (__|   <| | | | (_) | |  __/
|_.__/|_|\__,_|\___|_|\_\_| |_|\___/|_|\___|"""
        splash_text = urwid.Text(logo, align="left")
        company_name = urwid.Text(('splash_company', "by: Faraday"), align="left")
        version_text = urwid.Text(('splash_logo', f"v: {VERSION}"), align="left")
        splash_pile = urwid.Pile([splash_text, version_text, company_name])
        filler = urwid.Filler(urwid.Padding(splash_pile,  "center", ("relative", 40)))
        filler_attr = urwid.AttrMap(filler, "body")
        splash_view = filler_attr
        self.loop = urwid.MainLoop(splash_view, pop_ups=True,  screen=self.screen, palette=self.main_palette)
        self.loop.set_alarm_in(SPLASH_SCREEN_TIME, self.setup, user_data=initial_error_message)

    def draw_main_ui(self):
        logger.debug("Drawing UI")
        #Menu
        self.listbox = urwid.ListBox(EnvironmentWalker(self))
        self.listbox.offset_rows = 1
        # Footer
        self.statusbar_widget = urwid.Text("", align="left")
        statusbar_map = urwid.AttrMap(self.statusbar_widget, "foot")
        footer_items = [statusbar_map]
        self.help_popup_launcher = HelpPopUpLauncher(gui=self)
        self.target_search_popup_launcher = TargetSearchPopUpLauncher(gui=self)
        if self.engine.user.manual_ssh:
            self.manual_ssh_popup_launcher = ManulSSHPopUpLauncher(gui=self)
            footer_items.append((6, self.manual_ssh_popup_launcher))
        footer_items.append((7, self.help_popup_launcher))
        footer_items.append((9, self.target_search_popup_launcher))
        footer_items.append((7, urwid.Text("quit(Q)", align="left")))
        footer = urwid.AttrMap(urwid.Columns(footer_items, dividechars=1), "buttons_pane")
        # Header
        self.header_widget = urwid.Text(self.header_text, align="left")
        self.update_header_text()
        #popup
        self.notification_popup = NotificationPopUpLauncher(gui=self)
        notification_map = urwid.AttrMap(self.notification_popup, "buttons_pane")
        header_map = urwid.AttrMap(self.header_widget, "head")
        header = urwid.Columns([header_map, (1, notification_map)])
        self.loop.palette = self.main_palette
        self.loop.widget = urwid.Frame(urwid.AttrWrap(self.listbox, "body"), header=header, footer=footer)
        self.loop.unhandled_input = self.handle_key

    def draw_error_ui(self, title="", message=""):
        message_text = urwid.Text(message, align="center")
        message_pile = urwid.Pile([message_text, urwid.Text("\nPress any key to continue...", align="center")])
        message_box = urwid.LineBox(message_pile, title=title)
        message_box_attr = urwid.AttrMap(message_box, "notification")
        filler = urwid.Filler(urwid.Padding(message_box_attr, "center", ("relative", 30)))
        filler_attr = urwid.AttrMap(filler, "body")
        self.loop.widget = filler_attr
        self.loop.unhandled_input = lambda x: self.stop()

    def handle_key(self, key):
        if not urwid.is_mouse_event(key):
            if key in QUIT_KEYS:
                self.stop()
            elif key in HELP_KEYS:
               self.help_popup_launcher.open_pop_up()
            elif key in MANUAL_SSH_KEYS:
                self.show_manual_ssh_popup()
            elif key in SEARCH_KEYS:
                self.target_search_popup_launcher.open_pop_up()
        return True

    def show_notification(self, message):
        self.notification_popup.message = message
        self.notification_popup.open_pop_up()

    def start(self, initial_error_message):
        logger.debug("Gui started")
        try:
            self.draw_splash_screen(initial_error_message)
            self.loop.run()
        except KeyboardInterrupt as e:
            urwid.ExitMainLoop()
        except Exception as e:
            self.stop()
            raise

    def stop(self):
        logger.debug("Gui stopped")
        raise urwid.ExitMainLoop()

    def pause(self):
        logger.debug("Gui paused")
        self.ui_is_paused = True
        self.loop.stop()

    def restore(self):
        logger.debug("Gui restored")
        self.ui_is_paused = False
        self.loop.start()
        self.update_header_and_status()

    # Signals Callbacks

    def focus_changed(self):
        self.update_header_and_status()

    def start_connection(self, sender):
        self.statusbar_widget.set_text(("status_message", "Connecting..."))
        self.loop.draw_screen()
        try:
            self.engine.start_connection(sender.target_connection)
        except KilledByAdminException as e:
            self.show_notification("Your session was terminated by an Administrator")
            self.statusbar_widget.set_text(("status_message", ""))
        except Exception as e:
            self.show_notification(str(e))
            self.statusbar_widget.set_text(("status_message", ""))
        finally:
            pass

    def show_manual_ssh_popup(self):
        if self.engine.user.manual_ssh:
            self.manual_ssh_popup_launcher.open_pop_up()

