import logging
from collections import namedtuple

from .config import ConfigManager
from .exceptions import UnknownUser, BlackholeServerError
from .api_consumer import ApiConsumer

logger = logging.getLogger(__name__)

#API Endpoints
USER_DATA_URL = "/api/client/user/"
SECURE_SHELL_CONNECTION_URL = "/api/client/secure_shell/"
PRIVATE_KEY_URL = "/api/client/secure_shell/%s/private_key"
DATABASE_CONNECTION_URL = "/api/client/database/"
USER_ALLOWED_CONNECTION_URL = "/api/client/user/%s/allowed_to_connect/?target_id=%d"
SESSION_RECORD_URL = "/api/client/session_record/"
SESSION_RECORD_FINALIZE_URL = "/api/client/session_record/%s/ended/"

TargetDTO = namedtuple("TargetDTO", ["id", "name", "description", "connection_type", "environment"])
EnvironmentDTO = namedtuple("EnvironmentDTO", ['name'])


class PrivateKeyProxy(object):
    def __init__(self, private_key_data):
        self.key = private_key_data["id"]
        self.key_type = private_key_data["key_type"]
        self.private_key = private_key_data["private_key"]
        self.user = private_key_data["user"]
        self.environment = EnvironmentDTO(**private_key_data["environment"])

    def __str__(self):
        return f"Private Key:{self.key_type}  - {self.user} - {self.environment}"

    def readlines(self):
        pk_to_string = str(self.private_key).replace('\r', '')
        return pk_to_string.split('\n')


class TargetConnectionProxy(object):
    def __init__(self, id, name, ip, description, target_user, target):
        self.id = id
        self.name = name
        self.ip = ip
        self.description = description
        self.target_user = target_user
        self.target = target
        self.forward_x = False

    def __str__(self):
        return f"Id: {self.id} {self.name} {self.target.connection_type} {self.target_user}"

    def get_connection_data(self):
        full_target_connection = None
        if self.id != 0:
            if self.target.connection_type == "SSH":
                url = ApiConsumer.get_api_url(f"{SECURE_SHELL_CONNECTION_URL}{self.id}/")
                response = ApiConsumer.perform_get(url)
                if response.status_code == 200:
                    full_target_connection = response.json()
                else:
                    logger.error("Error in SecureShellConnection Api Response: %s", response.status_code)
            elif self.target.connection_type == "DB":
                url = ApiConsumer.get_api_url(f"{DATABASE_CONNECTION_URL}{self.id}")
                response = ApiConsumer.perform_get(url)
                if response.status_code == 200:
                    full_target_connection = response.json()
                else:
                    logger.error("Error in DatabaseConnection Api Response: %s", response.status_code)
        else:
            full_target_connection = {'id': self.id, 'target': {'id': self.id, 'name': self.name, 'ip': self.target.name, 'port': self.port, 'description': "", 'environment': {'name': ''}, 'device_type': 'Linux'}, 'authentication_method': 'Password', 'Password': {'username': self.target_user}, 'password': self.password}
        return full_target_connection

    def get_private_key(self):
        if self.target.connection_type == "SSH":
            logger.debug("Get Private Key for Target Connection: %s", self)
            url = ApiConsumer.get_api_url(PRIVATE_KEY_URL % self.id)
            response = ApiConsumer.perform_get(url)
            if response.status_code == 200:
                private_key_data = response.json()
                private_key = PrivateKeyProxy(private_key_data)
                logger.debug(private_key)
                return private_key
            else:
                raise BlackholeServerError("Missing Private Key")

class EnvironmentProxy(object):

    def __init__(self, name):
        self.name = name
        self.target_connections = []

    def get_target_connections(self):
        return self.target_connections


class ProfileProxy(object):
    def __init__(self, profile_data):
        self.id = None
        self.name = None
        self.environments = dict()
        self.load_data(profile_data)

    def load_target_connection(self, target_connection, connection_type):
        target_connection_id = target_connection["id"]
        target_connection_user = target_connection["user"]
        target = target_connection["target"]
        environment_name = target["environment"]["name"]
        if environment_name not in self.environments: self.environments[environment_name] = EnvironmentProxy(environment_name)
        environment_proxy = self.environments[environment_name]
        target_dto = TargetDTO(target["id"], target["name"], target["description"], connection_type, environment_name)
        target_connection_proxy = TargetConnectionProxy(target_connection_id, target["name"], target["ip"],
                                                        target["description"], target_connection_user,
                                                        target_dto)
        environment_proxy.target_connections.append(target_connection_proxy)

    def get_manual_target_connection(self, target_name, target_ip, target_port, target_user, password, connection_type):
        fake_target_dto = TargetDTO(0, target_ip, "", connection_type, "")
        fake_target_user = target_user
        fake_target_connection_proxy = TargetConnectionProxy(0, target_name, target_ip, "", fake_target_user, fake_target_dto)
        fake_target_connection_proxy.port = target_port
        fake_target_connection_proxy.password = password
        return fake_target_connection_proxy

    def load_data(self, profile_data):
        self.id = profile_data["id"]
        self.name = profile_data["name"]
        for target_connection in profile_data["secure_shell_connections"]:
            self.load_target_connection(target_connection, "SSH")

        for target_connection in profile_data["database_connections"]:
            self.load_target_connection(target_connection, "DB")


class UserProxy(object):
    def __init__(self, username):
        logger.debug("Create User Proxy")
        self.id = 0
        self.username = username
        self.first_name = None
        self.last_name = None
        self.enabled = None
        self.profile = None
        self.manual_ssh = False

    def __str__(self):
        return self.username

    def search_user(self):
        try:
            url = ApiConsumer.get_api_url(f"{USER_DATA_URL}{self.username}/")
            response = ApiConsumer.perform_get(url)
            if response.status_code == 200:
                logger.debug("User data (%s)", response.json())
                user_data = response.json()
                self.id = user_data["id"]
                self.first_name = user_data["first_name"]
                self.last_name = user_data["last_name"]
                self.enabled = user_data["enabled"]
                self.profile = ProfileProxy(user_data["profile"])
                self.manual_ssh = user_data["manual_ssh"]
                return user_data
            elif response.status_code == 404:
                raise UnknownUser(f"Unknown User: {self.username}")
            else:
                raise BlackholeServerError("Api Connection Error: %s" % response.status_code)
        except (UnknownUser, BlackholeServerError, ConnectionError):
            raise
        except Exception as e:
            logger.error("Error in get user data api call (%s)", e)
            raise Exception(f"Error searching user {self.username} in Blackhole Server ({e})")

    def get_full_name(self):
        return f"{self.last_name}, {self.first_name}"

    def get_environments(self):
        return list(self.profile.environments.values())

    def generate_session_record(self, session_uuid, client_pid, source_ip, log_file, target_connection):
        if target_connection.name == target_connection.ip:
            target_string = f"{target_connection.name}"
        else:
            target_string = f"{target_connection.name}"
        session_record_data = {"username": self.username,
                               "session_id": session_uuid,
                               "client_pid": client_pid,
                               "source_ip": source_ip,
                               "connection_type": target_connection.target.connection_type,
                               "target": target_string,
                               "target_user": target_connection.target_user,
                               "log_file": log_file}
        url = ApiConsumer.get_api_url(SESSION_RECORD_URL)
        response = ApiConsumer.perform_post(url, session_record_data)
        return response.json()["id"]

    def finalize_session_record(self, session_record_id):
        url = ApiConsumer.get_api_url(SESSION_RECORD_FINALIZE_URL % session_record_id)
        response = ApiConsumer.perform_get(url)
        if response.status_code == 200:
            logger.debug("Session Record Finalized OK: %s", response.json())
        else:
            logger.error("Error in finalize Session Record: %s - %s", response.status_code, response.text)

    def is_allowed_to_connect(self, target_id):
        if target_id != 0:
            url = ApiConsumer.get_api_url(USER_ALLOWED_CONNECTION_URL % (self.username, target_id))
            response = ApiConsumer.perform_get(url)
            return response.json()["allowed"]
        else:
            return True