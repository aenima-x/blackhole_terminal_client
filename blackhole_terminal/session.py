import logging
import socket

import paramiko

from .exceptions import KilledByAdminException, BlackholeServerError
from .client import SecureShellClient, DatabaseClient
logger = logging.getLogger(__name__)


class Session(object):

    CONNECTION_TYPE_SSH = "SSH"
    CONNECTION_TYPE_DB = "DB"

    def __init__(self, engine, target_connection):
        self.client = None
        self.engine = engine
        self.target_connection = target_connection
        self.connection_data = target_connection.get_connection_data()
        self.target_user = self.target_connection.target_user
        logger.debug("Connection Data: %s", self.connection_data)

    def start_session_client(self):
        raise NotImplementedError('Must Implement')

    def start(self):
        self.engine.session_started_handler(self)
        logger.info(
            f"[login] user={self.engine.user.username} as={self.target_user} to={self.target_connection.name} from={self.engine.source_ip} type={self.target_connection.target.connection_type} session_uuid={self.engine.session_uuid}")
        exception_in_client = None
        try:
            self.start_session_client()
        except Exception as e:
            exception_in_client = e
        finally:
            self.engine.session_ended_handler(self)
            logger.info(
                f"[logout] user={self.engine.user.username} as={self.target_user} to={self.target_connection.name} from={self.engine.source_ip} type={self.target_connection.target.connection_type} session_uuid={self.engine.session_uuid}")
        if exception_in_client:
            raise exception_in_client

    @classmethod
    def get_session(cls, engine, target_connection):
        if target_connection.target.connection_type == cls.CONNECTION_TYPE_SSH:
            return SecureShellSession(engine, target_connection)
        elif target_connection.target.connection_type == cls.CONNECTION_TYPE_DB:
            return DatabaseSession(engine, target_connection)
        else:
            logger.error("Unknown Connection Type: %s", target_connection.target.connection_type)
            raise ConnectionError(f"Session Error - Unknown Connection Type: {target_connection.target.connection_type}")


class SecureShellSession(Session):
    AUTHENTICATION_METHOD_PRIVATE_KEY = "Private Key"
    AUTHENTICATION_METHOD_PASSWORD = "Password"

    def __init__(self, engine, target_connection):
        super().__init__(engine, target_connection)
        self.client = SecureShellClient(self, target_connection.forward_x)
        logger.debug("SecureShellSession Created")

    @staticmethod
    def generate_private_key_file(private_key_proxy):
        logger.debug(private_key_proxy)
        if private_key_proxy.key_type == "RSA":
            private_key = paramiko.RSAKey.from_private_key(private_key_proxy)
        elif private_key_proxy.key_type == "DSA":
            private_key = paramiko.DSSKey.from_private_key(private_key_proxy)
        return private_key

    def start_session_client(self):
        try:
            terminal_size = self.engine.get_terminal_size()
            ip = self.connection_data["target"]["ip"]
            port = self.connection_data["target"]["port"]
            self.client.connect(ip, port, terminal_size)
        except socket.timeout as e:
            logger.error("Connection Failed <%s> [%s]", type(e), e)
            raise ConnectionError(f"Connection Error [{e}]")
        except Exception as e:
            logger.error("Connection Failed <%s> [%s]", type(e), e)
            raise
        try:
            if self.connection_data["authentication_method"] == self.AUTHENTICATION_METHOD_PRIVATE_KEY:
                private_key_proxy = self.target_connection.get_private_key()
                private_key = self.generate_private_key_file(private_key_proxy)
                self.client.start(self.target_user, private_key=private_key)
            elif self.connection_data["authentication_method"] == self.AUTHENTICATION_METHOD_PASSWORD:
                self.client.start(self.target_user, password=self.connection_data["password"])
            else:
                logger.error(f"Unknown Authentication Method ({self.connection_data['authentication_method']})")
                raise Exception(f"Unknown Authentication Method ({self.connection_data['authentication_method']})")
        except paramiko.AuthenticationException as e:
            self.client.stop()
            raise ConnectionError(f"Connection Error [{e}]")
        except KilledByAdminException as e:
            self.client.stop()
            raise
        except BlackholeServerError:
            self.client.stop()
            raise
        except Exception as e:
            msg = f"Unknown Error [{type(e)}] <{e}>"
            self.client.stop()
            raise Exception(msg)
        else:
            self.client.stop()


class DatabaseSession(Session):
    def __init__(self, engine, target_connection):
        super().__init__(engine, target_connection)
        self.client = DatabaseClient(self)
        logger.debug("Database Session Created")

    def get_connection_command(self):
        command = self.connection_data["command"]
        logger.debug("Running DB command {0}".format(command))
        return command

    def start_session_client(self):
        try:
            ip = self.connection_data["target"]["ip"]
            port = self.connection_data["target"]["port"]
            self.client.connect(ip, port)
        except socket.timeout as e:
            logger.error("Connection Failed <%s> [%s]", type(e), e)
            raise ConnectionError(f"Connection Error [{e}]")
        try:
            self.client.start()
        except KilledByAdminException as e:
            self.client.stop()
            raise
        except BlackholeServerError:
            self.client.stop()
            raise
        except Exception as e:
            msg = f"Unknown Error [{type(e)}] <{e}>"
            self.client.stop()
            raise Exception(msg)
        else:
            self.client.stop()


