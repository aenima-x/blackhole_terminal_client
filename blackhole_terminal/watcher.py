import signal
from queue import Queue
import logging
import stat
import threading
import os
import sys
from .config import ConfigManager
import redis

logger = logging.getLogger(__name__)


class StdOutHandler(object):
    def __init__(self):
        self.original_stdout = sys.stdout
        self.stdout_queue = Queue()
        self.stdout_sniffer = None
        logger.debug("StdOut Handler Created")

    def fileno(self):
        return self.original_stdout.fileno()

    def write(self, c):
        self.stdout_queue.put(c)
        self.original_stdout.write(c)

    def read(self, fd):
        data = os.read(fd, 1024)
        self.stdout_queue.put(data)
        return data

    def flush(self):
        self.original_stdout.flush()

    def capture(self, session_uuid, log_filename):
        logger.debug("Capture log: %s", log_filename)
        sys.stdout = self
        self.stdout_sniffer = StdOutSniffer(self.stdout_queue, session_uuid, log_filename)
        self.stdout_sniffer.start()

    def restore(self):
        if self.stdout_sniffer:
            sys.stdout = self.original_stdout
            self.stdout_sniffer.stop()
            self.stdout_sniffer.join()
            self.stdout_sniffer = None


class StdInHandler(object):
    def __init__(self):
        self.original_stdin = sys.stdin
        self.stdin_buffer = []
        logger.debug("StdIn Handler Created")

    def fileno(self):
        return self.original_stdin.fileno()

    def read(self, x):
        character = os.read(self.original_stdin.fileno(), 1)
        character_code = ord(character)
        if character_code == 127 and self.stdin_buffer:
            # remove last from buffer
            self.stdin_buffer.pop()
        elif character_code == 13:
            # analize buffer
            command = "".join(map(lambda x: x.decode('utf-8'), self.stdin_buffer))
            self.stdin_buffer.clear()
            logger.debug("Commad: %s", command)
        elif character_code == 3:
            # purge buffer
            self.stdin_buffer.clear()
        else:
            self.stdin_buffer.append(character)
        return character

    def capture(self):
        sys.stdin = self

    def restore(self):
        sys.stdin = self.original_stdin


class StdOutSniffer(threading.Thread):
    def __init__(self, stdout_queue, session_uuid, log_file_name):
        threading.Thread.__init__(self)
        self.session_uuid = session_uuid
        self.stdout_queue = stdout_queue
        self.log_file = None
        self.log_filename = log_file_name
        self.session_active = True
        self.redis_connection = None
        self.pubsub =  None
        self.redis_streaming_channel = None
        signal.signal(signal.SIGUSR1, self.start_streaming)
        signal.signal(signal.SIGUSR2, self.stop_streaming)
        logger.debug("Sniffer Created")

    def run(self):
        logger.debug("Sniffer Start")
        self.log_file = open(self.log_filename, "w")
        os.chmod(self.log_file.name, stat.S_IREAD | stat.S_IWRITE | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
        while True:
            if self.session_active:
                c = self.stdout_queue.get()
                if isinstance(c, bytes):
                    c = c.decode('utf-8', 'ignore')
                self.write_input(c)
                self.stdout_queue.task_done()
            else:
                self.log_file.close()
                logger.debug("Exit Sniffer thread")
                break

    def stop(self):
        logger.debug("Stop Sniffer")
        self.session_active = False
        self.stdout_queue.put("")
        if self.redis_streaming_channel:
            self.pubsub.close()

    def write_input(self, c):
        self.log_file.write(c)
        self.log_file.flush()
        os.fsync(self.log_file.fileno())
        if self.redis_streaming_channel:
            self.redis_connection.publish(self.redis_streaming_channel, c)

    def start_streaming(self, *args, **kwargs):

        redis_config = ConfigManager.get_config('redis')
        self.redis_connection = redis.Redis(host=redis_config.server, port=redis_config.port)
        self.pubsub = self.redis_connection.pubsub()
        self.redis_streaming_channel = redis_config.streaming_session_name % self.session_uuid
        logger.debug(f"Start Streaming on {self.redis_streaming_channel}")

    def stop_streaming(self, *args, **kwargs):
        if self.redis_streaming_channel:
            logger.debug(f"Stop Streaming on {self.redis_streaming_channel}")
            self.pubsub.close()
            self.redis_connection = None
            self.redis_streaming_channel = None

