paramiko==2.4.2
requests==2.22.0
redis==3.2.1
urwid==2.0.1
psutil==5.6.2
cryptography==2.4.2