#!/usr/bin/env python
import sys


def main():
    try:
        import blackhole_terminal
    except ModuleNotFoundError as e:
        sys.stderr.write(f"Blackhole Terminal Client: load ERROR! ({e})\n")
        sys.exit(1)
    except Exception as e:
        sys.stderr.write(f"Blackhole Terminal Client: ERROR [{e.__class__.__name__}({e})]\n")
        sys.exit(1)
    else:
        blackhole_terminal.launch()


if __name__ == "__main__":
    main()